﻿using MySql.Data.MySqlClient;
using System;
using System.Data;

namespace Medicalcenter.Data
{
    public class DBConnection
    {
        MySqlConnection mySqlConnection;
        public DBConnection()
        {
            mySqlConnection = new MySqlConnection("datasource=127.0.0.1;port=3306;database=medicalcenter;uid=root;pwd=1234;");
        }

        public bool ExecuteQuery(string query)
        {
            MySqlCommand mySqlCommand = new MySqlCommand(query,mySqlConnection);
            try
            {
                mySqlConnection.Open();
                mySqlCommand.ExecuteNonQuery();
                Console.WriteLine("con opren");
                return true;
            }catch(MySqlException ex)
            {
                Console.WriteLine(ex.ToString());
                return false;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }

        public DataTable GetQueryResults(string query)
        {
            MySqlCommand mySqlCommand = new MySqlCommand(query, mySqlConnection);
            try
            {
                mySqlConnection.Open();
                MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
                DataTable dataTable = new DataTable();
                if (mySqlDataReader.HasRows)
                {
                    dataTable.Load(mySqlDataReader);
                }
                return dataTable;
            }catch(MySqlException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
            finally
            {
                mySqlConnection.Close();
            }
        }
    }
}
