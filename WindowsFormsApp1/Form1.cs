﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MedicalCenter.Services;

namespace WindowsFormsApp1
{
    public partial class LoginScreen : Form
    {
        private AdminPanel adminPanel;
        private UserServices userServices;
        public LoginScreen()
        {
            adminPanel = new AdminPanel();
            userServices = new UserServices();
            InitializeComponent();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
        private void TxtPassword_OnValueChanged(object sender, EventArgs e)
        {
            txtPassword.isPassword = true;
        }

        private void BtnLogin_Click(object sender, EventArgs e)
        {
            if(!txtEmail.Text.Equals("") && !txtPassword.Text.Equals(""))
            {
                if (userServices.Login(txtEmail.Text, txtPassword.Text))
                {
                    this.Dispose();
                    adminPanel.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Wrong Email or Password","Wrong",MessageBoxButtons.OK,MessageBoxIcon.Error);
                    txtEmail.Text = null;
                    txtPassword.Text = null;
                }
            }            
        }
    }
}
