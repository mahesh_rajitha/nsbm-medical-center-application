﻿using MedicalCenter.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Doctor : Form
    {
        private AdminServices adminServices;
        public Doctor()
        {
            InitializeComponent();
            adminServices = new AdminServices();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Doctor_Load(object sender, EventArgs e)
        {
            string[] doctorDetails = adminServices.LoadDoctorDetails();
            if(doctorDetails.Length >0)
            {
                txtDocId.Text = doctorDetails[0];
                txtDoctorName.Text = doctorDetails[1];
                txtWorkingHospital.Text = doctorDetails[2];
            }
        }

        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (adminServices.UpdateDoctor(txtDocId.Text, txtDoctorName.Text, txtWorkingHospital.Text))
            {
                MessageBox.Show("Updated Successfully","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
        }
    }
}
