﻿using MedicalCenter.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AdminPanel : Form
    {
        AddStudent addStudent;
        private AdminServices adminServices;
        private Doctor doctor;
        public AdminPanel()
        {
            InitializeComponent();
            addStudent = new AddStudent();
            adminServices = new AdminServices();
            doctor = new Doctor();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BtnAddNewStudent_Click(object sender, EventArgs e)
        {
            addStudent.ShowDialog();
        }

        private void AdminPanel_Load(object sender, EventArgs e)
        {
            if(adminServices.GetStudentsTable() != null)
            {
                dgrStudentsTable.DataSource = adminServices.GetStudentsTable();
            }
        }

        private void BunifuFlatButton2_Click(object sender, EventArgs e)
        {
            doctor.ShowDialog();
        }
    }
}
