﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    static class Program
    {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            LoginScreen loginScreen = new LoginScreen();
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            loginScreen.ShowDialog();
            //Application.Run(new AdminPanel());
        }
    }
}
