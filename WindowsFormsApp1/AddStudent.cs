﻿using MedicalCenter.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class AddStudent : Form
    {
        private AdminServices adminServices;
        public AddStudent()
        {
            InitializeComponent();
            adminServices = new AdminServices();
        }

        private void BtnAddStudent_Click(object sender, EventArgs e)
        {
            if(!txtStudentId.Text.Equals("") && !txtStudentEmail.Text.Equals("") && !txtStudentName.Text.Equals("") && !txtStudentAge.Equals("") && !txtStudentDob.Equals(""))
            {
                if (adminServices.AddStudent(txtStudentId.Text, txtStudentEmail.Text, txtStudentName.Text, txtStudentAge.Text, txtStudentDob.Text))
                {
                    MessageBox.Show("Student Added To System","Success",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    txtStudentAge.Text = null;
                    txtStudentDob.Text = null;
                    txtStudentEmail.Text = null;
                    txtStudentId.Text = null;
                    txtStudentName.Text = null;
                }
            }
            else
            {
                MessageBox.Show("Please Fill All Fileds","Warning",MessageBoxButtons.OK,MessageBoxIcon.Warning);
            }
            
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
