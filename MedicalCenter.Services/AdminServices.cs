﻿using Medicalcenter.Data;
using System;
using System.Data;

namespace MedicalCenter.Services
{
    public class AdminServices
    {
        private DBConnection dBConnection;
        public AdminServices()
        {
            dBConnection = new DBConnection();
        }

        public bool AddStudent(string studentId,string studentMail,string studentName,string studentAge,string dob)
        {
            string query = "insert into students(studentId,studentMail,studentName,studentAge,dob) values('" + studentId+"','"+studentMail+"','"+studentName+"','"+studentAge+"','"+dob+"')";
            return dBConnection.ExecuteQuery(query);
        }

        public DataTable GetStudentsTable()
        {
            string query = "select * from students";
            DataTable dataTable = dBConnection.GetQueryResults(query);
            if(dataTable != null)
            {

                return dataTable;
            }
            return null;
        }

        public string[] LoadDoctorDetails()
        {
            string query = "select * from doctors";
            DataTable dataTable = dBConnection.GetQueryResults(query);
            if(dataTable != null)
            {
                string [] doctorDetail = new string[3];
                foreach(DataRow dataRow in dataTable.Rows)
                {
                    doctorDetail[0] = dataRow["id"].ToString();
                    doctorDetail[1] = dataRow["doctorName"].ToString();
                    doctorDetail[2] = dataRow["workingHospital"].ToString();
                }
                return doctorDetail;
            }
            return null;
        }

        public bool UpdateDoctor(string docId , string doctorName , string workingHospital)
        {
            string query = "update doctors set doctorName='"+doctorName+ "', workingHospital='"+workingHospital+"' where id='"+docId+"'";
            return dBConnection.ExecuteQuery(query);
        }

    }
}
